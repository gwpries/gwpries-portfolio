"use strict";

// Server engine for gpries.com portfolio. This node.js+react engine is reinventing the wheel in the most simple ways
// The only database used is for hit logging, other content is static.

// load required libraries /////////////////////////////////////////////////////
// express router
var express 	= require('express');
var app 		= express();

// filesystem interface
var fs 			= require('fs');

// React engine
var React 		= require('react');
var ReactDOMServer = require('react-dom/server');

// http parsing
var request 	= require('request');
var url 		= require('url');
var bodyParser 	= require('body-parser');

// ORM -- sqlite for simplicity
var Sequelize 	= require('sequelize');

// SMTP interface
var nodemailer 	= require('nodemailer');

// geoIP for redirection of undesirables
var geoip 		= require('geoip-lite');

// processing of time into language
var moment = require('moment');

// static html assets that are not managed by React 
var art = fs.readFileSync("/home/gwpries/assets/html/art.html");
var header = fs.readFileSync("/home/gwpries/assets/html/header.html");
var footer = fs.readFileSync("/home/gwpries/assets/html/footer.html");

// create the storage engine for hit logging 
var sequelize = new Sequelize('database', 'username', 'password', {
	host: 'localhost',
	dialect: 'sqlite',
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},
	// SQLite only
	storage: '/home/gwpries/portfolio.db'
});

// initialize the ORM schema
var Hits = sequelize.define('hits', {
	ip: {
		type: Sequelize.STRING,
	},
	code: {
		type: Sequelize.STRING,
	},
	geo: {
		type: Sequelize.STRING,	
	}
}, {
	freezeTableName: true // Model tableName will be the same as the model name
});

// full clearing and initialization of sql tables
// Hits.sync({force: true});

// regular init of sql tables
Hits.sync();

// load and init the left nav bar and footer elements, they are used on every page
require('./compiled/nav.js').init(React);
require('./compiled/footer.js').init(React);

// init the body parser for inbound POST requests
app.use(bodyParser.json({ limit: "5mb" }));

// main page
app.get('/', function (req, res) {
	return res.send(renderPage(req, res, {
		title: "Greg Pries",
		module: "portfolio"
	}));
});

// contact page
app.get('/contact', function (req, res) {
	var html = renderPage(req, res, {
		title: "Contact Information",
		module: "contact"
	});
	return res.send(html);
});

// projects overview
app.get('/projects', function (req, res) {
	var html = renderPage(req, res, {
		title: "Projects",
		module: "projects"
	});
	return res.send(html);
});

// work page
app.get('/work', function (req, res) {
	var html = renderPage(req, res, {
		title: "Work",
		module: "work"
	});
	return res.send(html);
});

// wp-admin fun for people that are poking around
app.get('/wp-admin', function (req, res) {
	var html = renderPage(req, res, {
		title: "Yeah Right!",
		module: "wpadmin"
	});
	return res.send(html);
});

// render a specific project
app.get('/project', function(req, res) {
	var inputs = getInputs(req);

	// bail out if no project specified
	if (!inputs['name']) {
		return handleError(res, "project not specified"); 
	}
	
	// load data file from disk
	var proj_src;
	try {
		proj_src = fs.readFileSync("/home/gwpries/assets/json/projects/" + inputs['name'] + ".json");
	} catch (ex) {
		return handleError(res, "project data file not found: " + inputs['name'] + ".json");
	}

	if (!proj_src) {
		res.setHeader('content-type', 'application/json');
		return res.send({"error":"project data not found"});
	}

	// parse project data into jason
	var proj = {};
	try {
		proj = JSON.parse(proj_src);
	} catch (ex) {
		return handleError("project data error: " + inputs['name'] + ".json");
	}
	
	// add project to be passed to react
	inputs['project'] = proj;

	// set project title
	inputs.pageTitle = "Project - " + inputs['project'].title;

	var html = renderPage(req, res, {
		title: "Yeah Right!",
		module: "project",
		inputs: inputs
	});
	return res.send(html);

});

// loader for project json data
app.get('/project_data', function (req, res) {
	var site = require('./compiled/site.js');
	var siteobj = site.init(React);

	var inputs = getInputs(req);

	if (!inputs['name']) {
		return handleError(res, "project not specified"); 
	}

	// load project
	var proj_src;
	try {
		proj_src = fs.readFileSync("/home/gwpries/assets/json/projects/" + inputs['name'] + ".json");
	} catch (ex) {
		res.setHeader('content-type', 'application/json');
		return res.send({"error":"project data file not found: " + inputs['name'] + ".json"});
	}

	if (!proj_src) {
		return handleError(res, "project data not found"); 
	}

	var proj = JSON.parse(proj_src);
	inputs['project'] = proj;

	// check to see if project is locked, and if viewer is approved
	var authen = checkAuth(inputs);

	// set output headers
	res.setHeader('content-type', 'application/json');

	// if the viewer is not authenticated, sanitize to default data
	var sanitized;
	if (proj.secure && (!authen || authen.error)) {
		proj = {
			title: proj.title,
			stub: proj.stub,
			cover: proj.cover,
			locked: true,
			secure: true
		};
	}	

	// for secure view, switch to an unblurred cover
	if (proj.secure && authen && authen.token) {
		if (proj.cover_secure) {
			proj.cover = proj.cover_secure;
		}
	}

	return res.send(proj);
});

// authorization routine for viewing private content
app.get('/auth', function (req, res) {
	res.setHeader('content-type', 'application/json');
	
	var inputs = getInputs(req);

	if (!inputs['password'] && !inputs['token']) {
		return res.send({"error":"password or token required"});
	}

	res.setHeader('content-type', 'application/json');

	var authen = checkAuth(inputs);
	res.send(authen);
});

// create the app server
var server = app.listen(3088, function () {
	console.log("React server running. 127.0.0.1:3088");
});

// quick token/password check for authorization routine
function checkAuth(inputs) {
	var auth = fs.readFileSync("/home/gwpries/assets/json/auth.json");
	var authj = JSON.parse(auth);
	if (inputs['password']) {
		if (authj[ inputs['password'] ]) {
			return {
				"token": authj[ inputs['password'] ]
			};
		} else {
			return {
				"error":"Incorrect Unlock Code"
			};
		}
	} else if (inputs['token']) {
		for (var k in authj) {
			if (authj[ k ] == inputs['token']) {
				return {
					"token":authj[ k ]
				};
			}
		}
		return {
			"error":"Incorrect Token"
		};
	}
}

// mail transport
function sendMail(subject, body) {
	var transporter = nodemailer.createTransport('smtps://localhost');

	// setup e-mail data with unicode symbols
	var mailOptions = {
	    from: 'system@gpries.com', // sender address
	    to: 'gwpries@gmail.com', // list of receivers
	    subject: subject, // Subject line
	    text: body
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        return console.log(error);
	    }
	    console.log('Message sent: ' + info.response);
	});
}

// main page runner routine
function renderPage(req, res, opts) {
	if (!opts) {
		opts = {};
	}

	var site = require('./compiled/site.js');
	var siteobj = site.init(React);

	var inputs;
	if (opts.inputs) {
		inputs = opts.inputs;
	} else {
		inputs = getInputs(req);
	}

	inputs.pageTitle = opts.title;

	// read the ip address of the client coming in from nginx, or directly
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	var geo = geoip.lookup(ip);
	if (geo && geo.country == "US" && geo.region == "MO") {
		// bad news redirect, not looking for jobs in Missouri and there are undesirables there
		// mail the violation and redirect
		sendMail("Portfolio Missouri Redirect!", "ip: " + ip);
		res.redirect(301, "https://milestepper.com/about");
	}

	// create a string dump of the geo data for logging
	var geo_string;
	if (geo) {
		geo_string = JSON.stringify(geo);
	}

	// log hit
	Hits.create({
		ip: ip,
		code: inputs.code,
		geo: geo_string
	});

	// an authorization code was given to view locked content
	if (inputs.code) {
		Hits.create({
			ip: ip,
			code: inputs.code
		});
		sendMail("New Authorized Portfolio View", "ip: " + ip + ", code: " + inputs.code);
	}

	// create default page title
	inputs.pageTitle = "Greg Pries - Portfolio";

	var Ready = React.createFactory(siteobj.Site);
	var html = ReactDOMServer.renderToString(Ready({
		module: opts.module,
		data: inputs
	}));

	// attach header
	html = getHeader() + html + getFooter();
	return html;
}

// handle error outputs
function handleError(res, error) {
	res.setHeader('content-type', 'application/json');
	return res.send({"error":error});
}

// routine to read GET / POST variables
function getInputs(req) {
	var inputs = {};
	for (var k in req.body) {
		inputs[k] = req.body[k];
		// console.log(k);
	}
	for (var k in req.query) {
		inputs[k] = req.query[k];
		// console.log(k);
	}
	if (inputs['unlock_code']) {
		inputs['code'] = inputs['unlock_code'];
	}

	return inputs;
}

// do any processing and assembly on the mostly-static header
function getHeader() {
	if (header) {
		header = header + ""; // convert to string
		header = header.replace(/(\r\n|\n|\r)/gm,""); // strip junk because react complains
	}
	return art + header;
}

// do any processing and assembly on the mostly-static footer
function getFooter() {
	return footer;
}
