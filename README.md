# Custom Portfolio #

[http://gpries.com](http://gpries.com)

This portfolio was created to showcase some of my personal projects, and to also show off some of my skills as a full-stack web developer. It is a simple, file-based content presentation system written in React and Node.js. The true benefits of React don't get to shine here, because most pages are only minimally interactive. More interactive pages are intended to be added later on as my portfolio grows. 

Using React's web components, I was able to focus on small sections individually before creating the main site layout. I could re-organize the site easily at any time with minimal code disruption.

### Features ###

* Server-side rendering, followed by a hand-off to the client which seamlessly continues page execution using the same code
* Easily re-configurable pages and a dozen different Lego-style components
* Gulp auto-build script
* Compiled and minimized assets
* Automatic installation of dependencies via "npm install"
* SCSS styling
* Easily installation on any Docker host (see additional project with Docker configuration and build system)

### File map ###

* /server.js /client.js: Raw JavaScript code
* /assets/jsx: JSX (A JavaScript/HTML hybrid) for creating each component
* /assets/css: SCSS style code. Each component has its own corresponding file
* /gulpfile.js: Build automation via Gulp

Thanks for looking!