"use strict";

//
// Create the project preview boxes
//

var Obj;
function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    var objects = {};

    Obj = React.createClass({
        displayName: "ProjectPreview",
        getInitialState: function () {
            var state = {
                password: ""
            };
            state.counter = 0;

            state.project = this.props.project;
            return state;
        },
        componentDidMount: function () {
            var r = this;

            // load project after browser initializes, this can be moved to getInitialState to support server-side rendering if that is desired
            // it is browser side because of possible authentication for now
            this.loadProject();
        },
        // loads the project json and supplies a password or token if it is available
        loadProject: function () {
            var r = this;

            var code = window.getParameterByName("code");
            var token;
            if (code) {
                console.log("unlocking via code:" + code);
            } else {
                if (window.localStorage) {
                    token = window.localStorage.getItem("token");
                }
            }
            if (this.props.siteRef.state.token) {
                token = this.props.siteRef.state.token;
            }

            // handle immediate tokens
            $.ajax({
                url: "/project_data",
                type: "GET",
                dataType: "json",
                data: {
                    name: this.props.name,
                    token: token,
                    password: code
                },
                success: function (data) {
                    if (data.error) {
                        r.setState({
                            project_error: data.error
                        });
                    } else {
                        r.setState({
                            project: data
                        });
                    }
                }
            });
        },
        // show the unlock project dialog
        showUnlock: function () {
            var r = this;
            this.props.siteRef.setState({
                unlockError: undefined
            }, function () {
                if (r.state.project.secure && !r.props.siteRef.state.unlocked) {
                    r.setState({
                        showUnlock: true
                    });
                }
            });
        },
        // perform the unlock action
        unlockAction: function () {
            var r = this;
            if (!this.state.password) {
                alert("unlock password is required");
                return;
            }
            r.props.siteRef.unlock(this.state.password);
        },
        // handle inputs into the password input and update the state
        changeHandler: function (event) {
            if (event.which && event.which == 13) {
                this.unlockAction();
                return;
            }
            if (event) {
                var name = $(event.target).attr("name");
                var value = event.target.value;
                var args = {};
                args[name] = value;
                this.setState(args);
            }
        },
        // focus the given element
        focus: function (element) {
            $(element).focus();
        },
        // render the project preview
        render: function () {
            if (this.state.project_error) {
                return React.createElement(
                    "div",
                    { className: "alert alert-danger" },
                    this.state.project_error
                );
            }

            if (!this.state.project) {
                return React.createElement(
                    "div",
                    null,
                    "Loading..."
                );
            }

            var style = {};

            style.backgroundImage = "url(" + this.state.project.cover + ")";

            var project_link = "/project/?name=" + this.state.project.stub;
            var target = "";

            return React.createElement(
                "div",
                { className: "gp-projectpreview", "data-stub": this.state.project.stub },
                React.createElement(
                    "a",
                    { href: project_link, onClick: this.showUnlock },
                    React.createElement(
                        "div",
                        { className: "panel panel-default" },
                        React.createElement(
                            "div",
                            null,
                            React.createElement("div", { className: "gp-x-cover", style: style })
                        ),
                        React.createElement(
                            "div",
                            { className: "panel-heading" },
                            React.createElement(
                                "div",
                                { className: "gp-x-flex" },
                                React.createElement(
                                    "div",
                                    { className: "gp-x-left" },
                                    React.createElement(
                                        "div",
                                        { className: "gp-x-name" },
                                        this.state.project.title
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "gp-x-category" },
                                        this.state.project.subtitle
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "gp-x-role" },
                                        this.state.project.role_short
                                    )
                                ),
                                React.createElement("div", { className: "gp-x-right" })
                            )
                        )
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init({
        bodyRef: bodyref,
        props: props
    });
};