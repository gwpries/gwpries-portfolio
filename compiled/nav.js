"use strict";

//
// Global nav bar, present on the left on browsers and expandable on mobile devices.
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Nav",
        render: function () {
            return React.createElement(
                "div",
                { className: "gp-nav" },
                React.createElement(
                    "ul",
                    null,
                    React.createElement(
                        "li",
                        { className: "gp-x-brand" },
                        React.createElement(
                            "a",
                            { href: "/" },
                            React.createElement("img", { className: "img-responsive", src: "/assets/images/gpcircleb.png" })
                        )
                    ),
                    React.createElement(
                        "li",
                        { className: "tooltipped tooltipped-e tooltipped-no-delay", "aria-label": "Projects" },
                        React.createElement(
                            "a",
                            { href: "/projects" },
                            React.createElement("i", { className: "icon-projects" }),
                            React.createElement(
                                "div",
                                { className: "gp-x-text" },
                                "Projects"
                            )
                        )
                    ),
                    React.createElement(
                        "li",
                        { className: "tooltipped tooltipped-e tooltipped-no-delay", "aria-label": "Work" },
                        React.createElement(
                            "a",
                            { href: "/work" },
                            React.createElement("i", { className: "icon-work" }),
                            React.createElement(
                                "div",
                                { className: "gp-x-text" },
                                "Work"
                            )
                        )
                    ),
                    React.createElement(
                        "li",
                        { className: "tooltipped tooltipped-e tooltipped-no-delay", "aria-label": "Contact" },
                        React.createElement(
                            "a",
                            { href: "/contact" },
                            React.createElement("i", { className: "icon-contact" }),
                            React.createElement(
                                "div",
                                { className: "gp-x-text" },
                                "Contact"
                            )
                        )
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};