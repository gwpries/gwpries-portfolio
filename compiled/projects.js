"use strict";

//
// List of all projects.
//

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var projects_to_show = ['thissite', 'cwpanel', 'cwwebapi', 'darkroom', 'cwcenter', 'milestepper', 'gmis2012', 'rhpos', 'nocview'];

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    // load the project preview class
    var ProjectPreview = require('projectpreview.js').init(React);

    var Obj = React.createClass({
        displayName: "Projects",
        getInitialState: function () {
            var state = {};
            state.projects = projects_to_show;
            state.counter = 0;
            return state;
        },
        // if the previews need to be reloaded, this will iterate over them and reload each one
        reload: function () {
            var r = this;
            $(".gp-projectpreview").each(function () {
                var stub = $(this).data("stub");
                if (r.refs[stub]) {
                    r.refs[stub].loadProject();
                } else {
                    console.log("projectpreview ref not found for " + stub);
                }
            });
        },
        // create the react object format for each project
        getProjects: function () {
            var r = this;
            if (this.state.projects) {
                var n = 0;
                return this.state.projects.map(function (project) {
                    n += 1;
                    var key = "project-" + n;
                    return React.createElement(
                        'li',
                        { key: key },
                        React.createElement(ProjectPreview, _extends({ ref: project }, r.props, { name: project }))
                    );
                });
            }
        },
        // render list of projects
        render: function () {
            return React.createElement(
                'div',
                { className: 'gp-projects' },
                React.createElement(
                    'ul',
                    null,
                    this.getProjects()
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};