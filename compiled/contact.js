"use strict";

//
// Contact component
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    var Nav = require('nav.js').init(React);
    var Footer = require('footer.js').init(React);
    var Hero = require('hero.js').init(React);

    var Obj = React.createClass({
        displayName: "Test",
        render: function () {
            return React.createElement(
                'div',
                { className: 'gp-contact' },
                React.createElement(Hero, null),
                React.createElement(
                    'div',
                    { className: 'gp-x-methods' },
                    React.createElement(
                        'ul',
                        null,
                        React.createElement(
                            'li',
                            null,
                            React.createElement('i', { className: 'icon-contact' }),
                            ' ',
                            React.createElement(
                                'a',
                                { href: 'mailto:gwpries@gmail.com' },
                                'gwpries@gmail.com'
                            )
                        ),
                        React.createElement(
                            'li',
                            null,
                            React.createElement('i', { className: 'icon-phone' }),
                            ' (404) 840-6473'
                        ),
                        React.createElement(
                            'li',
                            null,
                            React.createElement('i', { className: 'icon-linkedin' }),
                            ' ',
                            React.createElement(
                                'a',
                                { href: 'https://www.linkedin.com/in/greg-pries-9b360531', target: '_blank' },
                                'https://www.linkedin.com/in/greg-pries-9b360531'
                            )
                        )
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};