"use strict";

//
// Hit viewer for showing potential high value views -- not completed yet
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    var Obj = React.createClass({
        displayName: "Hits",
        render: function () {
            return React.createElement(
                "div",
                { className: "gp-template" },
                "Page not found"
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};