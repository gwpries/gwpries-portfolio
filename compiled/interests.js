"use strict";

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    var Nav = require('nav.js').init(React);
    var Footer = require('footer.js').init(React);

    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Test",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;
        },
        render: function () {
            return React.createElement(
                'div',
                { className: 'gp-interests' },
                React.createElement(
                    'ul',
                    null,
                    React.createElement(
                        'li',
                        null,
                        'Backpacking'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Photography'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Music'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Backpacking'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Kayaking'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Graphic design'
                    ),
                    React.createElement(
                        'li',
                        null,
                        'Programming'
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};