"use strict";

//
// A preview of the featured projects, include 3 or 4
//

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    // load the project preview class
    var ProjectPreview = require('projectpreview.js').init(React);

    // create the react class to hold each project preview object
    var Obj = React.createClass({
        displayName: "RecentProjects",
        reload: function () {
            var r = this;
            $(".gp-projectpreview").each(function () {
                var stub = $(this).data("stub");
                if (r.refs[stub]) {
                    r.refs[stub].loadProject();
                } else {
                    console.log("projectpreview ref not found for " + stub);
                }
            });
        },
        render: function () {
            return React.createElement(
                "div",
                { className: "gp-recentprojects" },
                React.createElement(
                    "div",
                    { className: "panel panel-bare gp-x-section" },
                    React.createElement(
                        "div",
                        { className: "panel-heading" },
                        React.createElement(
                            "div",
                            { className: "panel-title" },
                            "Recent Projects"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "panel-body" },
                        React.createElement(
                            "div",
                            { className: "row" },
                            React.createElement(
                                "div",
                                { className: "col-xs-12 col-sm-6 col-md-6" },
                                React.createElement(ProjectPreview, _extends({ ref: "milestepper" }, this.props, { name: "milestepper" }))
                            ),
                            React.createElement(
                                "div",
                                { className: "col-xs-12 col-sm-6 col-md-6" },
                                React.createElement(ProjectPreview, _extends({ ref: "nocview" }, this.props, { name: "nocview" }))
                            ),
                            React.createElement(
                                "div",
                                { className: "col-xs-12 col-sm-6 col-md-6" },
                                React.createElement(ProjectPreview, _extends({ ref: "rhpos" }, this.props, { name: "rhpos" }))
                            ),
                            React.createElement(
                                "div",
                                { className: "col-xs-12 col-sm-6 col-md-6" },
                                React.createElement(ProjectPreview, _extends({ ref: "gmis2013" }, this.props, { name: "gmis2012" }))
                            )
                        ),
                        React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "a",
                                { href: "/projects" },
                                "View more projects"
                            )
                        )
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};