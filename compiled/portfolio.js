"use strict";

//
// Main portfolio page, add in any components to be shown
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    var Nav = require('nav.js').init(React);
    var Footer = require('footer.js').init(React);
    var Hero = require('hero.js').init(React);
    var Skills = require('skills.js').init(React);
    var RecentProjects = require('recentprojects.js').init(React);

    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Portfolio",
        reload: function () {
            this.refs.recentprojects.reload();
        },
        render: function () {
            return React.createElement(
                'div',
                null,
                React.createElement(Hero, { siteRef: this.props.siteRef }),
                React.createElement(RecentProjects, { ref: 'recentprojects', siteRef: this.props.siteRef }),
                React.createElement(Skills, { siteRef: this.props.siteRef })
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init({
        bodyRef: bodyref,
        props: props
    });
};