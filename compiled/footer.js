"use strict";

//
// Global footer component
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    var Obj = React.createClass({
        displayName: "Footer",
        render: function () {
            var year = new Date().getFullYear();
            return React.createElement(
                "div",
                { className: "gp-footer" },
                React.createElement(
                    "div",
                    { className: "gp-x-flex" },
                    React.createElement(
                        "div",
                        { className: "gp-x-left" },
                        "© ",
                        year,
                        " Gregory W. Pries   ",
                        React.createElement(
                            "span",
                            { className: "hidden-xs" },
                            "|   Full Stack  ",
                            React.createElement(
                                "span",
                                { className: "hidden-sm" },
                                " /   DevOps   /   Systems"
                            )
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "gp-x-right" },
                        React.createElement(
                            "a",
                            { href: "/contact" },
                            "Contact"
                        )
                    )
                )
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};