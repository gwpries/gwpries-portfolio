"use strict";

//
// Every time I visit a site I like, I hit /wp-admin to see if its just a wordpress site. A little easter egg.
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Test",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;
        },
        render: function () {
            return React.createElement(
                "div",
                { className: "gp-wpadmin" },
                "Come on, Wordpress? This portfolio was made from the ground up using Node.js and React. I realize it's ridiculous to create a platform for a single portfolio, but I did it to practice engine creation and some isomorphic js techniques. Thanks for looking!"
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};