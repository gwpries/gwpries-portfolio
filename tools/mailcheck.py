#!/usr/bin/python

# tool to check for a valid email address on a remote mailserver
import socket
import subprocess
import sys

if not len(sys.argv) > 1:
	print "email is required as argv1"
	sys.exit()

email = sys.argv[1]
email_parts = sys.argv[1].split("@")
person = email_parts[0]
domain = email_parts[1]

# domain = sys.argv[1]
cmd = ["/usr/bin/host","-t","mx",domain]

proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)

for line in proc.stdout:
	mx_line = line.rstrip('\n')
	break

mx_parts = mx_line.split()
mx_parts = mx_parts[::-1]

mx_server = mx_parts[0].rstrip('/\.$/')

print "attempting to find valid acct (" + email + ") on mail server " + mx_server

# this is the mx server, now to telnet to it
server_info = (mx_server, 25)

socket = socket.socket()
socket.connect(server_info)
response = socket.recv(1024)

print "SEND: EHLO gpries.com"
socket.send("EHLO gpries.com\r\n")
response = socket.recv(1024)
# print response

print "SEND: MAIL FROM: <gpries@gpries.com>"
socket.send("MAIL FROM: <gpries@gpries.com>\r\n")
response = socket.recv(1024)
# print response

print "SEND: RCPT TO: <" + email + ">"
socket.send("RCPT TO: <" + email + ">\r\n")
response = socket.recv(1024)
# print "mailserver response: " + response

if "550" in response:
	print "email not found"
else:
	print email + " found!"

socket.close()