// primary client entry point. 

// prep react
var React = require('react');
var ReactDOM = require('react-dom');

// load the entire site
var site = require('site.js');

// wait until the document is ready before proceeding with taking over the react code from the server
$( document ).ready(function() {
    var props = {};
    var props_doc = document.getElementById("in-props");
   
    if (props_doc) {
            var inprops = props_doc.innerHTML;
            props = JSON.parse(inprops);
    }

    var siteobj = site.init(React, ReactDOM, props);
});
