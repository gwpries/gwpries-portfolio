var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cssnano	= require('gulp-cssnano');
var order = require("gulp-order");
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var print = require('gulp-print');
var babel = require('gulp-babel');
var autoprefixer = require('gulp-autoprefixer');
var stripDebug = require('gulp-strip-debug');
var browserify = require('gulp-browserify');
var exec = require('child_process').exec;
var cache = require("gulp-cached");
var filelog = require('gulp-filelog');
var gulpsync = require('gulp-sync')(gulp);

// define the source scss files
var scss = [
	"assets/css/variables.scss",
	"assets/css/site.scss",
	"assets/css/*.scss",
];

// scss compilation task
gulp.task('scss', function() {
	gulp.src(scss)
		.pipe(concat('site.scss'))
		.pipe(sass().on('error', sass.logError))
		.pipe(cssnano())
		.pipe(concat('site.css'))
		.pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
		.pipe(gulp.dest('./compiled'));
});

// define the source jsx files
var jsx = [
	'assets/jsx/*.jsx'
];

// jsx compilation
gulp.task('jsx', function() {
	return gulp.src(jsx)
		.pipe(cache('linting'))
		.pipe(babel({
			presets: ['react'],
			compact: false
		})).on('error', errorHandler)
		.pipe(filelog())
		.pipe(gulp.dest('./compiled'));
});

// use browserify to package all runtime code into a single file
gulp.task('browserify', function() {
	return gulp.src('./client.js')
		.pipe(
			browserify({
				insertGlobals : true,
				paths: ['./plugins/react-dev', './compiled/','./node_modules','/usr/lib/node_modules'], // dev
				// paths: ['./plugins/react', './compiled/','./node_modules','/usr/lib/node_modules'], // live
			})
		)
		.pipe(concat('bundle.js'))
		.pipe(gulp.dest('./compiled'));
});

// when the single bundle is finished, use the command line version of uglify to compress it. The gulp version is
// too RAM heavy and makes gulp bog down over time
gulp.task('browserifymin', function() {
	exec(
		'uglifyjs compiled/bundle.js compiled/msinternal_dev.js > compiled/bundle.min.js'
	);
});

// files to watch that will cause forever to restart the app server
var watchrestart = [
	'client.js',
	'server.js',
	'assets/json/projects.json',
	'assets/html/header.html',
	'assets/html/footer.html'
];

// task which causes the app server to restart
gulp.task('reloadreact', function() {
	exec(
		'forever restart /home/gwpries/server.js',
		function (err, stdout, stderr) {
		    console.log(stdout);
		    console.log(stderr);
    	}
	);
});

// sequentially run jsx, browserify, and react
gulp.task('syncbatch', 
	gulpsync.sync(
		[
			'jsx',
			'browserify',
			'browserifymin',
			'reloadreact'
		]
	)
);

// the watch routines, watch all files and trigger for changes
gulp.task('watch', function() {
	gulp.watch(scss, ['scss']);
	gulp.watch(jsx, { interval: 500 }, ['syncbatch']);
	gulp.watch(watchrestart, { interval: 500 }, ['reloadreact']);
});

// set up all the tasks
gulp.task('default', [
	'watch',
	'scss',
	'syncbatch'
]);

function errorHandler (error) {
	console.log(error.toString());
	this.emit('end');
}
