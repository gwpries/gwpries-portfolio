"use strict";

// 
// Project viewer, including photoswipe image viewer
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Project",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;

            // add in the photoswipe container
            $(".photoswipe-container").append('<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>');
            
            // call to the server to load the project json
            this.loadProject();
        },
        loadProject: function() {
            var r = this;

            var code = window.getParameterByName("code");
            var token;
            if (code) {
                console.log("unlocking via code:" + code);
                // this.unlock(code);
            } else {
                if (window.localStorage) {
                    token = window.localStorage.getItem("token");
                }
            }
            if (this.props.siteRef.state.token) {
                token = this.props.siteRef.state.token;
            }

            // handle immediate tokens
            $.ajax({
                url: "/project_data",
                type: "GET",
                dataType: "json",
                data: {
                    name: this.props.data.name,
                    token: token,
                    password: code,
                },
                success: function(data) {
                    if (data.error) {
                        r.setState({
                            project_error: data.error
                        });
                    } else { 
                        r.setState({
                            project: data
                        }, function() {
                            console.log("get sizes");
                            r.getSizes();
                        });
                    }
                }
            });    
        },
        // detect sizes of images, photoswipe requires a height and width to show properly
        getSizes: function() {
            var r = this;
            if (this.state.project.cover) {
                var img = new Image();
                img.addEventListener("load", function(){
                    var width = this.naturalWidth;
                        var height = this.naturalHeight;
                        r.state.project.cover_height = height;
                        r.state.project.cover_width = width;
                
                        r.setState({
                            project: r.state.project
                        });
                });
                img.src = this.state.project.cover;
            }

            if (this.state.project.photos) {
                this.state.project.photos.map(function(photo) {
                    var img = new Image();
                    img.addEventListener("load", function(){
                        var width = this.naturalWidth;
                        var height = this.naturalHeight;
                        photo.height = height;
                        photo.width = width;
                        console.log("setting photo height: " + height + " width: " + width);
                        r.setState({
                            project: r.state.project
                        });
                    });
                    img.src = photo.url;
                });
            }
        },
        // lay out all of the additional photos below the mian project photo
        getAdditionalPhotos: function() {
            var r = this;
            if (this.state.project.photos) {
                var n = 0;
                return this.state.project.photos.map(function(photo) {
                    n += 1;
                    var key = "photo-" + n;
                    var style = {

                    };
                    style.backgroundImage = "url(" + photo.url + ")";

                    return (
                        <li key={key}>
                            <a href="javascript:" onClick={r.photoSwipe.bind(r, photo)}>

                                <div className="gp-photo gp-photo-thumb" src={photo.url} data-caption={photo.caption} data-full={photo.url} data-height={photo.height} data-width={photo.width} style={style}>

                                </div>
                            </a>
                        </li>
                    );
                });
            }
        },
        // execute the photoswipe gallery
        photoSwipe: function(photo) {
            var pswpElement = document.querySelectorAll('.pswp')[0];
            var p_items = [];

            var start_index = 0;
            var i = 0;
            $(".gp-photo").each(function() {
                if ($(this).data("full") && $(this).data("height") && $(this).data("width")) {
                    var max = 1200;
           
                    var link = $(this).data("full");
                    var id = $(this).data("id");

                    p_items.push({
                        src: link,
                        w: $(this).data("width"),
                        h: $(this).data("height"),
                        title: $(this).data("caption"),
                    });
                    if (photo.url == $(this).data("full")) {
                        start_index = i;
                    }
                    i++;
                }
            });
        
            // define options (if needed)
            var options = {
                index: start_index,
                escKey: true
            };

            // Initializes and opens PhotoSwipe            
            if (typeof(PhotoSwipe)) {
                var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, p_items, options);
                gallery.init();
                // r.setState
                // self.current.obj = gallery;
            } else {
                console.log("photoswipe error");
            }
        },
        // preprocessor to iterate over given skills to create the react based list
        getSkills: function() {
            if (this.state.project && this.state.project.skills) {
                var n = 0;
                return this.state.project.skills.map(function(skill) {
                    n += 1;
                    var key = "skill-" + n;
                    return (
                        <li key={key}>{skill}</li>
                    );
                });
            }
        },
        // preprocessor to iterate over used components to create the react based list
        getComponents: function() {
            if (this.state.project && this.state.project.components) {
                var n = 0;
                return this.state.project.components.map(function(component) {
                    n += 1;
                    var key = "component-" + n;
                    return (
                        <li key={key}>{component}</li>
                    );
                });
            }
        },
        // returns an object that can be used to inject raw html from the description (to support <br>)
        getHtmlDescription: function() {
            if (this.state.project) {
                return {
                    __html: this.state.project.description
                };
            }
        },  
        // main page renderer
        render: function() {
            if (this.state.project_error) {
                return (
                    <div className="alert alert-danger">{this.state.project_error}</div>
                );
            }
            if (!this.state.project) {
                return (
                    <div>Loading...</div>
                );
            }

            return (
                <div className="gp-project">
                    <div className="gp-x-flex">
                        <div className="gp-x-left">
                            <div className="gp-x-cover-photo">
                                <a href="javascript:" onClick={this.photoswipe}>
                                    <img src={this.state.project.cover} data-full={this.state.project.cover} data-width={this.state.project.cover_width} data-height={this.state.project.cover_height} className="gp-photo img-responsive" onClick={this.photoSwipe}/>
                                </a>
                            </div>
                            <div className="gp-x-additional-photos">
                                <ul>
                                    {this.getAdditionalPhotos()}
                                </ul>
                            </div>
                        </div>
                        <div className="gp-x-right">
                            <div className="panel panel-underline">
                                <div className="panel-heading">
                                    <div className="panel-title">
                                        Project Name
                                    </div>
                                </div>
                                <div className="panel-body">
                                    {this.state.project.title}
                                    <div className="gp-x-subtitle">
                                        {this.state.project.subtitle}
                                    </div>
                                </div>
                            </div>

                            <div className="panel panel-underline">
                                <div className="panel-heading">
                                    <div className="panel-title">
                                        Client
                                    </div>
                                </div>
                                <div className="panel-body">
                                    {this.state.project.client}
                                </div>
                            </div>

                            <div className="panel panel-underline">
                                <div className="panel-heading">
                                    <div className="panel-title">
                                        Description
                                    </div>
                                </div>
                                <div className="panel-body" dangerouslySetInnerHTML={ this.getHtmlDescription() }>
                                </div>
                            </div>

                            <div className="panel panel-underline">
                                <div className="panel-heading">
                                    <div className="panel-title">
                                        My Role
                                    </div>
                                </div>
                                <div className="panel-body">
                                    {this.state.project.role}
                                </div>
                            </div>

                            <div className="panel panel-underline">
                                <div className="panel-heading">
                                    <div className="panel-title">
                                        Applied Skills
                                    </div>
                                </div>
                                <div className="panel-body">
                                    <ul>
                                        {this.getSkills()}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.props.data.project.test}
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
