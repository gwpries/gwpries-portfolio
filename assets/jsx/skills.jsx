"use strict";

var skills = {
    stack: [
        'Experience building entire sites (including this one) with modern front-end frameworks such as React and Angular',
        'Strong artistic technical background',
        '16 years experience working with the entire stack from metal to mobile',
        'Advanced knowledge of optimizing databases on both operating system, and data structural levels',
        'Fluent in over a dozen programming languages and frameworks',
        'Strong emphasis on good graphic design and a superior user experience',
        'Graphic design experience including web friendly vector art',
    ],
    devops: [
        'Integrating cloud platforms, Amazon AWS services, including EC2 and S3',
        'Application containers such as Docker',
        'Designing virtualized hosting and application platforms',
        'Performance tuning and optimization for virtual platform limitations',
        'Strong emphasis on automation when possible',
        'Code and configuration distribution platforms such as Chef, Puppet and Ansible',
        'Ability to solve problems quickly using combinations of scripting languages and APIs',
    ],
    syseng: [
        'Designing end-to-end hosting and application platforms',
        'Planning for future growth based on leading indicators and past performance',
        'Ability to envision the entire system and make detailed decisions on individual components',
        'Strong understanding of networks, routing hardware and connectivity protocols',
        'Experience with designing preventative security systems and performing data forensics on intrusions',
    ],
}
function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    var Obj = React.createClass({
        displayName: "Skills",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;
        },
        getSkills: function(type) {
            if (skills[ type ]) {
                var n = 0;
                return skills[ type ].map(function(skill) {
                    n += 1;
                    var key = "skill-" + n;
                    return (
                        <li key={key}>
                            {skill}
                        </li>
                    );
                });
            }
        },
        render: function() {
            return (
                <div className="gp-skills">
                    <div className="panel panel-bare gp-x-section">
                        <div className="panel-heading">
                            <div className="panel-title">
                                Areas of Expertise
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-12 col-md-4">
                                     <div className="panel panel-bare">
                                        <div className="panel-heading">    
                                            <div className="panel-title">
                                                Full Stack Development
                                            </div>
                                        </div>
                                        <div className="">
                                            <ul>
                                                {this.getSkills("stack")}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xs-12 col-md-4">
                                    <div className="panel panel-bare">
                                        <div className="panel-heading">    
                                            <div className="panel-title">
                                                DevOps
                                            </div>
                                        </div>
                                        <div className="">
                                            <ul>
                                               {this.getSkills("devops")}
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xs-12 col-md-4">
                                    <div className="panel panel-bare">
                                        <div className="panel-heading">    
                                            <div className="panel-title">
                                               Systems Engineering
                                            </div>
                                        </div>
                                        <div className="">
                                            <ul>
                                                {this.getSkills("syseng")}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
