"use strict";

// 
// A simple test components that demonstrates the live nature
// 

var Test;
var ProjectPreview;

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    var objects = {};

    ProjectPreview = require("projectpreview.js").init(React, ReactDOM);

    // alert("payment dialog init");
    Test = React.createClass({
        displayName: "Test",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;
            var int_id = setInterval(function() {
                r.setState({
                    counter: r.state.counter + 1
                }, function() {
                    if (r.state.counter > 300) {
                        clearInterval(r.state.int_id);
                    }
                });
            }, 100);
            r.setState({
                 int_id: int_id
            });
        },
        render: function() {
            return (
                <span className="cw-test">
                    This is a web page. With a counter: {this.state.counter}
                    <div>
                        <ProjectPreview />
                    </div>
                </span>
            );
        }
    });
    return Test;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init({
        bodyRef: bodyref,
        props: props
    });
};
