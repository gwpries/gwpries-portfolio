"use strict";

// 
// Global nav bar, present on the left on browsers and expandable on mobile devices.
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Nav",
        render: function() {
            return (
                <div className="gp-nav">
                    <ul>
                        <li className="gp-x-brand">
                            <a href="/">
                                <img className="img-responsive" src="/assets/images/gpcircleb.png"/>
                            </a>
                        </li>
                        <li className="tooltipped tooltipped-e tooltipped-no-delay" aria-label="Projects">
                            <a href="/projects">
                                <i className="icon-projects"/>
                                <div className="gp-x-text">
                                    Projects
                                </div>
                            </a>
                        </li>
                        <li className="tooltipped tooltipped-e tooltipped-no-delay" aria-label="Work">
                            <a href="/work">
                                <i className="icon-work"/>
                                <div className="gp-x-text">
                                    Work
                                </div>
                            </a>
                        </li>
                        <li className="tooltipped tooltipped-e tooltipped-no-delay" aria-label="Contact">
                            <a href="/contact">
                                <i className="icon-contact" />
                                <div className="gp-x-text">
                                    Contact
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
