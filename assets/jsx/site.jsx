"use strict";

var Site;

//
// Main site container
//

function init(inargs) {
	if (!inargs) {
	    inargs = {};
	}
	var objects = {};

	var Nav = require('nav.js').init(React);
    var Footer = require('footer.js').init(React);

    Site = React.createClass({
		displayName: "Site",
		getInitialState: function () {
			var state = {};

		    if (this.props.data) {
	            state = this.props.data;
		    }

			var bodyRef;
			
			// this are listed out individually because Browserify does not like variables when it is creating its bundle.js
			// more experimentation should be done to find a cleaner progmatic router.
			if (this.props.module == "test") {
		        bodyRef = require('test.js').init(React);
		    } else if (this.props.module == "portfolio") {
		        bodyRef = require('portfolio.js').init(React);
		    } else if (this.props.module == "contact") {
		        bodyRef = require('contact.js').init(React);
		    } else if (this.props.module == "work") {
		        bodyRef = require('work.js').init(React);
		    } else if (this.props.module == "interests") {
		        bodyRef = require('interests.js').init(React);
		    } else if (this.props.module == "projects") {
		        bodyRef = require('projects.js').init(React);
		    } else if (this.props.module == "wpadmin") {
		        bodyRef = require('wpadmin.js').init(React);
		    } else if (this.props.module == "hits") {
		        bodyRef = require('hits.js').init(React);
		    } else if (this.props.module == "project") {
		        bodyRef = require('project.js').init(React);
		    }

		    if (state.projects) {
		    	state.projects_byname = {};
		    	state.projects.map(function(proj) {
		    		state.projects_byname[ proj.stub ] = proj;
		    	});
		    }

		    state.bodyRef = bodyRef;

		    return state;
		},
		// unlock routine that sends a password or token to the server and checks if it is authorized
		unlock: function(password, token) {
            var r = this;
            if (!password && !token) {
                alert("unlock password  or token is required");
                return;
            }
            $.ajax({
                url: "/auth",
                type: "GET",
                dataType: "json",
                data: {
                    password: password,
                    token: token,
                },
                success: function(data) {
                    if (data.token) {
                        r.setState({
                            unlocked: true,
                            token: data.token,
                        }, function() {
	                        // reload every shown project preview now that we are authenticated
	                        if (r.refs.siteBody && r.refs.siteBody.reload) {
	                        	r.refs.siteBody.reload();
	                        }

	                        // also set long term memory
	                        if (window.localStorage) {
	                            window.localStorage.setItem("token", data.token);
	                        }
	                    });
                    } else {
                        r.setState({
                            unlockError: data.error
                        });
                    }
                }
            });    
		},
		// when the browser begins, check to see if an authorization code was given, and unlock any secret projects
		componentDidMount: function () {
			var r = this;
		    var code = window.getParameterByName("code");
		    if (code) {
		    	console.log("unlocking via code:" + code);
		    	this.unlock(code);
		    } else {
			    if (window.localStorage) {
			    	var token = window.localStorage.getItem("token");
			    	if (token) {
				    	r.unlock(null, token);
				    }
			    }
			}
		},
		// action to show the menu
		showMenu: function() {
			this.setState({
				menuShowing: true,
			});
		},
		// action to hide the menu
		hideMenu: function() {
			this.setState({
				menuShowing: false,
			});
		},
		render: function() {
		    var Body;
		    
		    // For whatever page we're showing, install the reference as Body
		    if (this.state.bodyRef) {
	            Body = this.state.bodyRef;
		    }

		    // serialize the arguments when we are the server, so they can be easily read by the client once it is passed over
		    var json = JSON.stringify(this.props);

		    var propStore = <script type="application/json" id="in-props" dangerouslySetInnerHTML={{__html: json}}></script>;

		    var navmenu_class = "gp-x-menu";
		    if (this.state.menuShowing) {
		    	// menu is showing now
		    } else {
		    	// menu is not showing (on mobile), so hide it
		    	navmenu_class += " hidden-xs";
		    }
			return (
			    <div className="gp-portfolio">
				    <div className="gp-portfolio-inner">
	                    <div className="gp-x-page">
	                    	{ !this.state.menuShowing ?
		                    	<div className="gp-x-mobile-menu visible-xs">
		                    		<div className="gp-x-button" onClick={this.showMenu}>
		                    			<i className="icon-menu"/>
		                    		</div>
		                    	</div>
	                    	: null }

	                    	{ this.state.menuShowing ?
	                    		<div className="gp-x-mobile-menu-mask" onClick={this.hideMenu}>

	                    		</div>
	                    	: null }

	                    	<div className={navmenu_class}>
	                            <Nav/>
	                        </div>
	                        <div className="gp-x-left hidden-xs">
	                           
	                        </div>
	                        <div className="gp-x-right">
	                        	<div className="gp-x-page-title">
	                        		<table>
	                        			<tbody>
		                        			<tr>
		                        				<td>
					                        		<div className="gp-x-page-title-text">
					                        			{ this.state.pageTitle ?
					                        				<span>{this.state.pageTitle}</span>
					                        			: 
					                        				<span>&nbsp;</span> 
					                        			}
					                        		</div>
					                        	</td>
					                        </tr>
				                        </tbody>
				                    </table>
	                        	</div>
	                            <div className="gp-x-page-inner container">
	                                { Body ?
					                    <Body className="gp-site" ref="siteBody" data={this.state} siteRef={this} />
						            : 
						            	<span>Page not found</span> 
						           	}
	                            </div>
	                            <Footer/>
	                        </div>
	                    </div>

	                </div>
		            {propStore}
			    	<div className="photoswipe-container"></div>
			    </div>
			);
        }
    });

	objects.Site = Site;

	// If this is a browser and not being rendered on the server, go ahead and start the program running, and also read in any given URL arguments
	if (typeof window !== 'undefined') {
    	window.getParameterByName = function(name, url) {
		    if (!url) url = window.location.href;
		    name = name.replace(/[\[\]]/g, "\\$&");
		    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		        results = regex.exec(url);
		    if (!results) return null;
		    if (!results[2]) return '';
		    return decodeURIComponent(results[2].replace(/\+/g, " "));
		}

        objects.main = ReactDOM.render(
            <Site {...inargs.props}/>,
            document.getElementById("gp-root")
        );
    }

    return objects;
}

// jsx go in heres

var React;
var ReactDOM;
var bodyref;
module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;


    return init({
        bodyRef: bodyref,
        props: props
    });
};
