"use strict";

// 
// Contact component
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    var Nav = require('nav.js').init(React);
    var Footer = require('footer.js').init(React);
    var Hero = require('hero.js').init(React);

    var Obj = React.createClass({
        displayName: "Test",
        render: function() {
            return (
                <div className="gp-contact">
                    <Hero/>
                    <div className="gp-x-methods">
                        <ul>
                            <li>
                                <i className="icon-contact"/> <a href="mailto:gwpries@gmail.com">gwpries@gmail.com</a>
                            </li>
                            <li>
                                <i className="icon-phone"/> (404) 840-6473
                            </li>
                            <li>
                                <i className="icon-linkedin"/> <a href="https://www.linkedin.com/in/greg-pries-9b360531" target="_blank">https://www.linkedin.com/in/greg-pries-9b360531</a>
                            </li>
                        </ul>
                    </div>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
