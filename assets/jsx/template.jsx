"use strict";

// 
// A template to use for creating future components
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    // alert("payment dialog init");
    var Obj = React.createClass({
        displayName: "Test",
        getInitialState: function () {
            var state = {};
            state.counter = 0;
            return state;
        },

        componentDidMount: function () {
            var r = this;
        },
        render: function() {
            return (
                <div className="gp-template">
                    Page
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
