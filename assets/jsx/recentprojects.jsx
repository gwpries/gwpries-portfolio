"use strict";

// 
// A preview of the featured projects, include 3 or 4
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    // load the project preview class
    var ProjectPreview = require('projectpreview.js').init(React);

    // create the react class to hold each project preview object
    var Obj = React.createClass({
        displayName: "RecentProjects",
        reload: function() {
            var r = this;
            $(".gp-projectpreview").each(function() {
                var stub = $(this).data("stub");
                if (r.refs[ stub ]) {
                    r.refs[ stub ].loadProject();
                } else {
                    console.log("projectpreview ref not found for " + stub);
                }
            });
        },
        render: function() {
            return (
                <div className="gp-recentprojects">
                    <div className="panel panel-bare gp-x-section">
                        <div className="panel-heading">
                            <div className="panel-title">
                                Recent Projects
                            </div>
                        </div>

                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-12 col-sm-6 col-md-6">    
                                    <ProjectPreview ref="milestepper" {...this.props} name="milestepper" />
                                </div>
                                <div className="col-xs-12 col-sm-6 col-md-6">
                                    <ProjectPreview ref="nocview" {...this.props} name="nocview" />
                                </div>
                                <div className="col-xs-12 col-sm-6 col-md-6">
                                    <ProjectPreview ref="rhpos" {...this.props} name="rhpos" />
                                </div>
                                <div className="col-xs-12 col-sm-6 col-md-6">
                                    <ProjectPreview ref="gmis2013" {...this.props} name="gmis2012" />
                                </div>
                            </div>
                            <div>
                                <a href="/projects">View more projects</a>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
