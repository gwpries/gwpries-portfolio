"use strict";

// 
// This component is the main hero image, with the personal photo and title
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }

    // Create main React object  
    var Obj = React.createClass({
        displayName: "Hero",
        render: function() {
            return (
                <div className="gp-hero">
                    <div className="gp-x-profile">
                        <div>
                            <img className="gp-x-profile-img" src="/assets/images/talledega.jpg"/>
                        </div>
                        <div className="gp-x-name">
                            Greg Pries - <span className="location">Atlanta, GA</span>
                        </div>
                        <div className="gp-x-title">
                            Front-end Engineer, Full Stack Developer, DevOps Engineer
                        </div>
                        <div className="gp-x-statement text-left">
                            Full stack experience, with a passion for solving problems, new technology, and pushing the limits of modern interfaces. Good design and smooth UX are principals reflected in everything I do.
                        </div>
                    </div>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
