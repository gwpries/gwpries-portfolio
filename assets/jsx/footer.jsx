"use strict";

// 
// Global footer component
// 

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    var Obj = React.createClass({
        displayName: "Footer",
        render: function() {
            var year = new Date().getFullYear();
            return (
                <div className="gp-footer">
                    <div className="gp-x-flex">
                        <div className="gp-x-left">
                            © {year} Gregory W. Pries &nbsp; <span className="hidden-xs">| &nbsp; Full Stack &nbsp;<span className="hidden-sm"> / &nbsp; DevOps &nbsp; / &nbsp; Systems</span></span>
                        </div>
                        <div className="gp-x-right">
                            <a href="/contact">Contact</a>
                        </div>
                    </div>
                </div>
            );
        }
    });
    return Obj;
}

var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
