"use strict";

//
// Work history component.
//

function init(inargs) {
    if (!inargs) {
        inargs = {};
    }
    
    // this can be offloaded into a json file, but for now it is just as easy to list it here
    var work = [
        {
            company: "Cyber Wurx, LLC",
            company_link: "https://cyberwurx.com",
            title: "Sr. Systems Engineer",
            location: "Atlanta, GA",
            start_year: 2008,
            end_year: "Current",
            description: "Cyber Wurx is a co-location and internet service provider operating a 20,000 square foot datacenter in Atlanta, GA. Responsible for all internal business integrations, oversight, and growth of the client hosting platform. ",
            logo: "/assets/images/cwlogo.png",
            accomplishments: [
                'Created portal for clients to manage and extend existing hosting services.',
                'Oversaw and directed operations for over 1000 managed Linux servers.',
                'Assisted with specifications for current and future service offerings based on capacity, growth forecasts and expenses.',
                'Designed network metering system linking client quotas with billing for automatic overage reporting and non-payment suspensions.',
                'Created "NOCView", a dedicated dashboard for viewing all critical components of the datacenter, including telemetry sensors, security cameras, emergency tickets and external events such as inclement weather.',
                'Implemented a multi-region VOIP network connecting multiple states to a singular phone network. This liberated the company from the legacy analog phone network, saving tens of thousands per year.',
                'Created a custom network-wide traffic sniffer identifying high-risk clients performing bulk email operations.',
                'Designed and rolled out a custom IDS (Intrusion Detection System) encompassing all hosting servers and specific attack signatures. Linux Kernel and PHP modules were created for enhanced logging ability of high-risk applications.',
                'Handled migration and integration for a new billing platform. Transition of existing services, including full automation of service cancellation and suspension.',
                'Designed a global private API for integration of all company applications. An elementary programmer with a sufficient level API key could control physical power to servers, read network transfer rates, suspend clients, retrieve facility temperatures, provision new services, and more than a hundred other functions specific to the operation of Cyber Wurx.',
                'Migrated services for over 16,000 IP addresses due to an address block re-allocation',
                'Prompted and oversaw the migration from hardware servers to VPS, including creation of an AWS style VPS platform for resale. Company infrastructure was reduced from 2 cabinets to a single 2U server.', 
                'Implemented automatic server tracking system which will discover a server based on network location and automatically inventory all components, power outlet id, location id, network id, and customer information.',
                'Created Android App with integrated bluetooth scanner for tracking of customer shipments, facility walkthroughs, automated label printing and customer visitation check-in.',
                'Integrated a live event stream into the Cyber Wurx company chat room. As real-time events happened, a bot would inform chat participants. Supported events include incoming phone calls with complete customer information, support tickets, BGP and network events, network traffic alarms and more.',
            ],
        },
        {
            company: "MileStepper, LLC",
            company_link: "https://milestepper.com",
            title: "Technical Co-founder",
            location: "Marietta, GA",
            start_year: 2014,
            end_year: "Current",
            logo: "/assets/images/mslogo.png",
            description: "Technical Co-founder of MileStepper.com, a social network connecting outdoor enthusiasts and assisting with planning adventures. As the technical co-founder, this role encompases design and implementation of all aspects of technology for the Company. MileStepper uses cutting edge technology and serves as a test bed for learning new skills and techniques.",
            accomplishments: [
                'Designed and programmed frontend, backend, database, internal and external API integrations.',
                'Created cutting-edge React/Node isomorphic javascript engine. Pages are rendered on the server, and then passed to the client (web browser) mid-execution resulting in a fast, seamless user experience.',  
                'Implemented cloud-based hosting on Amazon AWS with geographically based content distribution for faster load times and improved scaling.',
                'Integrated over a dozen third-party APIs for data collection and providing a rich user experience.',
                'Created administrative dashboard to aid in visualizing and achieving growth goals and user satisfaction',
                'Responded to direct user feedback in regards to problems and suggestions.',
            ],
        },
        {
            company: "Red Hare Brewing Company",
            company_link: "http://www.redharebrewing.com/",
            title: "Retail Sales Development",
            location: "Marietta, GA",
            start_year: "2012",
            end_year: "Contract",
            logo: "/assets/images/rhlogo.jpg",
            description: "Contract to design, implement, and support a custom Android-based point of sale system for retail sales. The system processes credit and debit cards through a low-cost merchant account rather than using pre-packaged high-percentage payment systems. Additional features include integration with inventory stock and custom sales reports. Any amount of terminals may be added based on client demand at no additional software expense.",
            accomplishments: [
                'Created Java code base using Google Android frameworks.',
                'Implemented full PCI compliance by working with Sage Payments to approve code.',
                'Integrated third-party transaction processing via REST API and HMAC-SHA1 signed HTTP requests.',
                'Scaled from one terminal to unlimited based on growing client demand.',
                'Cloud based zero-cost syncing with no requirement for an on-site server.',
                'Ability to run on commodity hardware to ensure operation long into the future.',
            ],
        },
        {
            company: "Cyber Wurx, LLC",
            company_link: "https://cyberwurx.com",
            title: "Systems Administrator / Support Technician",
            location: "Atlanta, GA",
            logo: "/assets/images/cwlogo.png",
            start_year: 2000,
            end_year: 2008,
            description: "Administered a Debian Linux hosting cluster and assisted in growing from 50 servers to over 1000. Created tools and procedures for automatic tracking and provisioning of new hardware, handling client requests and service migrations. Extensive resource troubleshooting of third party software, and hardware bottlenecks.",
            accomplishments: [
                'Integrated monitoring system with alarms and on-call escalation',
                'Created PXE network-based auto-installer for fast privisioning of new servers. Over 20 operating systems supported and network installed from a Linux based network bootloader.',
                'Pioneered a key-based network backup system which allowed secure, remote backups via ssh & rsync to central backup servers.',
                'Direct customer problem resolution including performance troubleshooting and other issues',
            ],
        },
    ];

    var Job;
    Job = React.createClass({
        displayName: "Job",
        getAccomplishments: function() {
            var n = 0;
            return this.props.job.accomplishments.map(function(accomplishment) {
                n += 1;
                var key = "accomp-" + n;
                return (
                    <li key={key}>
                        {accomplishment}
                    </li>
                );
            });
        },
        render: function() {
            return (
                <div className="gp-work-job">
                    <div className="panel panel-underline">
                        <div className="panel-heading">
                            <div className="panel-title">
                                <div className="gp-x-flex">
                                    <div className="gp-x-logo">
                                        <a href={this.props.job.company_link} target="_blank"><img src={this.props.job.logo} className="img-responsive"/></a>
                                    </div>
                                    <div className="gp-x-left">
                                        <span className="gp-x-title">{this.props.job.title}</span>
                                        <div className="gp-x-company">
                                            <a href={this.props.job.company_link} target="_blank">{this.props.job.company}</a> - {this.props.job.location}
                                        </div>
                                    </div>
                                    <div className="gp-x-right">
                                        {this.props.job.start_year}

                                        { this.props.job.end_year ?
                                            <span> - {this.props.job.end_year}</span>
                                        : null }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="gp-x-description">
                                {this.props.job.description}
                            </div>
                            <div className="gp-x-accomplishments">
                                <ul>
                                    {this.getAccomplishments()}
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            );
        }
    });

    // The primary Work object that is a series of Jobs
    var Obj = React.createClass({
        displayName: "Work",
        getJobs: function() {
            var n = 0;
            return work.map(function(job) {
                n += 1;
                var key = "job-" + n;

                return (
                    <Job key={key} job={job}/>
                );
            });
        },
        render: function() {
            return (
                <div className="gp-work">
                    {this.getJobs()}
                </div>
            );
        }
    });
    return Obj;
}



var React;
var ReactDOM;
var bodyref;

module.exports.init = function (react, reactdom, props) {
    React = react;
    ReactDOM = reactdom;

    return init();
};
