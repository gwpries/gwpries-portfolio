{
	"title": "GMIS Conference Adventure Game, St. Simons Island",
	"stub": "gmis2012",
	"cover": "https://milestepper.com/img/aws/milestepper-user-uploads/146343928082213850351637306681o.jpg1461961083.jpg/750",
	"subtitle": "Live-action 'Amazing Race' style team-building event",
	"client": "Government Management Information Sciences (GMIS), Georgia Chapter",
	"description": "<i>Escape from Margaritaville</i> was an educational team-building exercise for the 2012 Georgia GMIS summer conference. The organization sought to teach government IT leaders about problem solving with available technical resources, and contacted me to create a game fitting these requirements. The catch: It must fit the conference theme, Jimmy Buffet.<br><br>For the concept, I thought back to my past: playing adventure games as a kid. In all of those games, the player roamed around a virtual world, picking up items, and using them to solve puzzles. What if we could pull off the same thing, but augmented into the real world? I wanted to bring the magic feeling of that ah-ha moment to this event.<br><br>Participants were teamed up in groups of 5, each receiving an Android tablet with mobile data service, an inventory sack, a golf cart, and free reign to race all over St Simons Island, GA.<br><br>The main game application was based on a Google Maps engine with custom, geographically accurate, illustrated map tiles. The cartoon-esque map led participants around an augmented reality version of St. Simons Island. As challenges were completed and user proximity to key locations changed, the map evolved to advance the game. The first team to complete all challenges and escape the island became the winner.",
	"role": "Project Lead, Systems Engineer, Full Stack Development. Designing a string of parallel compehensive interactive puzzles is the perfect example of systems integration.",
	"skills": [
		"[Frontend Programming] Java (Android)",
		"[Frontend Programming] Live dashboard (HTML5)",
		"[Graphic Design] Map illustration & game sprites",
		"[Graphic Design] Prop design",
		"[DevOps] Rollout of frontend game application to all tablets, and backend server to VPS.",
		"[DevOps] Automated processing of master map PSD to individual map tiles at multiple zoom levels allowing even last-minute edits.",
		"[Backend Programming] Secure server backend. No cheating.",
		"[Database] MySQL game state and reporting team telemetry."
	],
	"components": [
		"Android/Java Application",
		"Backend Server Development",
		"Cloud Hosting of Application",
		"Drawing GPS-correct Map Overlays",
		"VOIP Integration",
		"Puzzle Design & Creation",
		"Execution (Mission Control)"
	],	
	"photos": [
		{
			"url": "/assets/images/gmis/61.jpg",
			"caption": "The game intro. Participants each received a tablet and were walked through a brief tutorial"
		},
		{
			"url": "/assets/images/gmis/gameplay.jpg",
			"caption": "General gameplay. As players drove around St. Simons Island, they picked up items at the package icons. After an item was scanned in, it would be added to their virtual inventory. Every item could be combined with other items -- in real life -- in order to solve real physical puzzles. A solved puzzle would always yield a QR code that the tablet could scan to advance the game. Scanning a new answer code let you advance the game."
		},
		{
			"url": "/assets/images/gmis/missioncontrol.jpg",
			"caption": "Back at Mission Control, I had a projector display in the room with each team and their status. I helped teams by sending them hints and answer questions. On my admin tablet, I could view all teams as they roamed around the map, and tap to speak with them directly."
		},
		{
			"url": "/assets/images/gmis/hoylepuzzle.jpg",
			"caption": "Finding an item: a card catalog card. Players figured out that they should head to the library, and then found the answer to their first puzzle. This unlocked 3 more puzzles on the island which caused the teams to spread out for load balancing. The library had never seen such excitement."
		},
		{
			"url": "/assets/images/gmis/rubiks.jpg",
			"caption": "Another puzzle. Players were expected to use a rubiks cube solver app to solve the cube and scan the QR code."
		},
		{
			"url": "/assets/images/gmis/placemat.jpg",
			"caption": "For the provided lunch, we directed them to a restaurant. When they sat down for lunch, this placemat was put down. The participants had previously picked up a marker. By the time they finished their lunch, they had figured out how to proceed."
		},
		{
			"url": "/assets/images/gmis/phonepuzzle.jpg",
			"caption": "Lunch was concluded with a check, and a fortune cookie. I had custom fortune cookies created that all contained a phone number. If you called the number from your mobile phone, you'd receive a 'You cannot call from THIS number' message. Eventually they were lead to the only payphone on the island, and they had previously picked up two quarters as an item. They combined the number from the fortune cookie, with the two quarters and the payphone, to receive instructions: Look in the phone book."
		},
		{
			"url": "/assets/images/gmis/workstation.jpg",
			"caption": "My workstation at the hotel. I could only complete about 80% of the game before getting to the island. A lot of it had to be created based on the current conditions and finding locations for items. The game engine was made to be flexible."
		},
		{
			"url": "/assets/images/gmis/props.jpg",
			"caption": "Puzzle props all ready to go out in the morning"
		},
		{
			"url": "/assets/images/gmis/5789.jpg",
			"caption": "The final stage of the game, a puzzle mailbox containing plane tickets to get off the island. As players completed puzzles, they picked up keys along the way. None of the keys opened this mailbox. It had a trick magnetic trap door, and the first item they started the game with was a magnet. The answer they needed, they had all along."
		},
		{
			"url": "/assets/images/gmis/players.jpg",
			"caption": "2 teams completed the game successfully at a race to the finish, and everyone had a great time."
		}

	],
	"year": 2016
}